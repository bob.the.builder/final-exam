class OrdersController < ApplicationController
    def index
        @order = Order.all
    end
    
    def new
        @order = Order.new
    end
    
    def create
        @order = current_cashier.orders.new(order_params)
        @order.save
        new_price = compute(@order)
        @order.update_attribute :total_price, new_price
        redirect_to @order
    end
    
    def order_summary 
        @order = Order.new(order_params)
        @order.total_price = 0
    end
    
    def delete
        @order = Order.find(params[:id])
    end
    
    def destroy
       @order = Order.find(params['id'])
       @order.destroy
       redirect_to orders_path
    end
    
    def show
        @order = Order.find(params['id'])
    end
    
    private
    def order_params
        params.require(:order).permit( :total_price, :cash, orderlines_attributes: [:id, :_destroy, :price, :product_id], )
    end
    def compute(a)
        pricee = 0.0
        a.orderlines each do |b|
            b.price = b.product.price * b.quantity.to_d
            pricee += b.price
        end
        return pricee
    end
end
