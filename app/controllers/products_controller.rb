class ProductsController < ApplicationController
    before_action :authenticate_admin!
        def new
        @product = Product.new
    end
    
    def index
        @product = Product.all
    end
    
    def create
        @product = Product.new(prod_params)
        @product.admin = current_admin
        @product.save
        redirect_to @product
    end
    
    def edit
        @product = Product.find(params['id'])
        if @product.admin != current_admin
            redirect_to products_path
        end
    end
    
    def update
        @product = Product.find(params['id'])
        @product.update(prod_params)
        @product.save
        redirect_to products_path
    end
    
    def delete
        @product = Product.find(params[:id])
    end
    def destroy
        @product = Product.find(params['id'])
        @product.destroy
        redirect_to products_path
    end
    
    def show
        @product = Product.find(params['id'])
    end
    
    private
    def prod_params
        params.require(:product).permit(:name, :price, :status)
    end
end
