class Product < ApplicationRecord
    belongs_to :admin
    has_many :orderlines
end
