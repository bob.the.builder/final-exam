class Order < ApplicationRecord
    belongs_to :cashier
    attr_accessor :orderlines_attributes
    has_many :orderlines
    has_many :products
    accepts_nested_attributes_for :orderlines , reject_if: :all_blank, allow_destroy: true
end
