Rails.application.routes.draw do
  devise_for :customers, only: [:sessions], path: 'customers'
  devise_for :cashiers, only: [:sessions], path: 'cashiers'
  devise_for :admins, only: [:sessions], path: 'admins'
    resources :products
    authenticate :admin do
      resources :cashiers, only: [:new, :create, :edit, :update, :destroy, :delete]
    end
    authenticate :cashier do
      resources :products, only: [:index]
    end
    
    resources :cashiers, only: [:index, :show]
    resources :public
    resources :orders do
      collection do
        post 'order_summary'
      end
    end
    resources :customers 

    post 'about', to: "public#about", as: "about"
    #post 'order_summary', to: "orders#order_summary", as: post
    root to: "public#home"
    
end
